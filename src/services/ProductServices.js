import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const GetProduct = (url) => {
  const [product, setProduct] = useState({});

  useEffect(() => {
    axios.get(url).then((res) => {
      setProduct(res.data);
    });
  }, []);

  return product;
};

const GetAllCategory = () => {
  const [category, setCategory] = useState();

  useEffect(() => {
    axios.get("https://fakestoreapi.com/products/categories").then((res) => {
      setCategory(res.data);
    });
  }, []);

  return category;
};

const Login = (body) => {
  axios.post("https://fakestoreapi.com/auth/login", body).then((res) => {
    localStorage.setItem("token", res.data.token);
    return true;
  });
};

const GetCartItems = () => {
  const [cartItems, setCartItems] = useState(0);
  axios.get("https://fakestoreapi.com/carts/1").then((res) => {
    setCartItems(res.data.products.length);
  });

  return cartItems;
};

export default {
  GetProduct,
  GetAllCategory,
  Login,
  GetCartItems,
};
