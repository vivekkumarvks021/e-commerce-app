import axios from "axios";
import { useEffect, useState } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import "./App.css";
import Filter from "./components/Filter";
import Header from "./components/Header";
import Login from "./components/Login";
import ProductDetails from "./components/ProductDetails";
import ProductList from "./components/ProductList";
import Cart from "./components/Cart";
import ProductServices from "./services/ProductServices";

function App() {
  const [products, setProducts] = useState([]);

  const GetAllProduct = (category) => {
    let url = "https://fakestoreapi.com/products";

    if (category !== "all") {
      url = `https://fakestoreapi.com/products/category/${category}`;
    }

    axios.get(url).then((res) => {
      setProducts(res.data);
    });
  };

  const navigate = useNavigate();

  const fetchProductDetails = (id) => {
    navigate(`product-details/${id}`);
  };

  const filterProducts = (category) => {
    GetAllProduct(category);
  };

  useEffect(() => {
    GetAllProduct("all");
  }, []);

  return (
    <div>
      <Header navigate={navigate} />

      <Routes>
        <Route
          path="/"
          element={[
            <Filter filterProducts={filterProducts} />,
            <ProductList
              productList={products}
              fetchProductDetails={fetchProductDetails}
            />,
          ]}
        />
        <Route
          path="/product-details/:productId"
          element={<ProductDetails />}
        />
        <Route path="login" element={<Login />} />
        <Route path="cart" element={<Cart />} />
      </Routes>
    </div>
  );
}

export default App;
