import React, { useState } from "react";

function Cart() {
  let cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
  let discount = 40;
  let deliveryCharge = 50;
  const [cartItem, setCartItem] = useState(cartItems);
  const removeFromCart = (id) => {
    let newCart = cartItem.filter((cart) => {
      return cart.cartId !== id;
    });

    setCartItem(newCart);
    localStorage.setItem("cartItems", JSON.stringify(newCart));
  };

  const calculatePrice = () => {
    let price = 0;
    cartItem.forEach((item) => {
      price = price + item.price;
    });

    return price;
  };

  const clearAll = () => {
    setCartItem([]);
    localStorage.setItem("cartItems", JSON.stringify([]));
  };

  const calculateTotalPrice = () => {
    let price = calculatePrice();
    return price - discount + deliveryCharge;
  };

  let cartDetails = cartItem?.map((item, index) => {
    return (
      <div className="cart-item" key={index}>
        <div className="cart-image-container">
          <img className="cart-item-image" src={item.image} />
          <div className="cart-quantity">
            <button className="btn">-</button>
            <span className="cart-item-qty">1</span>
            <button className="btn">+</button>
          </div>
        </div>
        <div>
          <p className="item-title">{item.title}</p>
          <p className="item-price cart-item-price">Price :- ${item.price}</p>
          <button
            className="btn btn-accent cart-btn"
            onClick={() => {
              removeFromCart(item.cartId);
            }}
          >
            Remove From Cart
          </button>
        </div>
      </div>
    );
  });
  let priceDetails = (
    <div className="price-details">
      <span className="bold">Price Details({cartItem.length})</span>
      <div className="item-price price-block">
        <div>Price :-</div>
        <div>${calculatePrice()}</div>
      </div>
      <div className="price-block">
        <div>Discount :- </div>
        <div>${discount}</div>
      </div>
      <div className="price-block">
        <div>Delivery Charge :-</div>
        <div> ${deliveryCharge}</div>
      </div>
      <div className="price-block">
        <div className="bold">Total Price :-</div>
        <div>${calculateTotalPrice()}</div>
      </div>
      <div className="price-block clear-cart">
        <button className="btn btn-secondary" onClick={clearAll}>
          Clear All Cart
        </button>
        <button className="btn">Place Your Order</button>
      </div>
    </div>
  );
  return (
    <div className="cart">
      <div className="cart-details">My Cart {cartDetails}</div>
      {priceDetails}
    </div>
  );
}

export default Cart;
