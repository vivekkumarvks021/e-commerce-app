import React from "react";
import ProductServices from "../services/ProductServices";

const Filter = ({ filterProducts }) => {
  let all = (
    <>
      <button className="btn" onClick={() => filterProducts("all")}>
        All
      </button>
    </>
  );
  let category = ProductServices?.GetAllCategory();
  const otherFilter = category?.map((data, index) => {
    return (
      <button className="btn" onClick={() => filterProducts(data)} key={index}>
        {data}
      </button>
    );
  });

  return (
    <div>
      {all} {otherFilter}
    </div>
  );
};

function GetFilterData(data) {
  ProductServices?.GetFilteredProduct();
}

export default Filter;
