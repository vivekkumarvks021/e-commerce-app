import React from "react";
import "../App.css";
import ProductServices from "../services/ProductServices";

function Header({ navigate }) {
  const checkToken = () => {
    let token = localStorage.getItem("token");
    return token ? "Vivek" : "Login";
  };
  return (
    <div>
      <header className="header">
        <div className="header-logo">
          <h2>Amazon</h2>
        </div>
        <div>
          <button
            className="btn"
            onClick={() => {
              if (checkToken() === "Login") navigate("login");
            }}
          >
            {checkToken()}
          </button>
          <button className="btn" onClick={()=>{navigate("cart")}}>
            Cart({ProductServices.GetCartItems()})
          </button>
          {/* <svg>
            <use href="../images/cart-icon.png"></use>
          </svg> */}
        </div>
      </header>
    </div>
  );
}

export default Header;
