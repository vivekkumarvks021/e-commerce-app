import React, { useState } from "react";

function ProductList({ productList, fetchProductDetails }) {
  let cartItems = JSON.parse(localStorage.getItem("cartItems")) || []
  const [cart, setCart] = useState(cartItems);

  const handleCart = (product) => {
    let cartId = cart.length + 1;
    let newCart = [...cart, {...product, cartId: cartId}];
    setCart(newCart);
    localStorage.setItem("cartItems", JSON.stringify(newCart))
  };
  
  let product = productList.map((product) => {
    return (
      <div className="product card" key={product.id}>
        <img className="product-image" src={product.image} alt="" />
        <p
          className="item-name"
          onClick={() => {
            fetchProductDetails(product.id);
          }}
        >
          {product.title}
        </p>
        <p className="item-price">Price:- {product.price}$</p>
        <div className="action-btn">
          <button
            className="btn btn-primary cart-btn"
            onClick={() => handleCart(product)}
          >
            Add to cart
          </button>
          <button className="btn btn-secondary cart-btn">Buy Now</button>
        </div>
      </div>
    );
  });
  return (
    <div>
      <div className="productList">{product}</div>
    </div>
  );
}

export default ProductList;
