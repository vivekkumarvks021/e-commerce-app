import React from "react";
import { useParams } from "react-router-dom";
import GetProduct from "../services/ProductServices";

function ProductDetails() {
  let { productId } = useParams();
  let url = `https://fakestoreapi.com/products/${productId}`;
  let productDetails = GetProduct.GetProduct(url);

  let product = (
    <div className="row">
      <div className="column">
        <img className="product-image" src={productDetails?.image} alt="" />
      </div>
      <div className="column">
        <p>{productDetails?.title}</p>
        <p>{productDetails?.description}</p>
        <span>Ratings:{productDetails?.rating?.rate}</span>
        <div>
          <p>Price: {productDetails.price}$</p>
        </div>
      </div>
    </div>
  );
  return <div>{product}</div>;
}

export default ProductDetails;
